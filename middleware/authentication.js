export default function ({store, redirect, route}) {
    const userIsLoggedIn = store.getters.isLoggedIn;
    const urlProfile = /^\/profile(\/|$)/.test(route.fullPath);
    const urlLogin = /^\/login(\/|$)/.test(route.fullPath);
    const urlRegister = /^\/register(\/|$)/.test(route.fullPath);

    if (!userIsLoggedIn && urlProfile) {
        return redirect('/login');
    }

    if (userIsLoggedIn && (urlLogin || urlRegister)) {
        return redirect('/profile');
    }

    return Promise.resolve()
}