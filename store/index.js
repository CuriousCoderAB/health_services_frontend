const Cookie = process.client ? require('js-cookie') : undefined;
const cookieparser = process.server ? require('cookieparser') : undefined;

export const state = () => ({
    navHeight: 0,
    accessToken: null,
    isSubmitting: false,
    flashMessages: [],
    user: null,
    showTerms: false,
    showPrivacy: false
});
export const getters = {
    isLoggedIn(state) {
        return !!state.accessToken;
    },
    hasUser(state) {
        return !!state.user;
    },
    userIsConfirmed(state) {
        return !!state.user && state.user.hasConfirmed;
    },
    isSubmitting(state) {
        return state.isSubmitting;
    },
    isShowingTerms(state) {
        return state.showTerms;
    },
    isShowingPrivacy(state) {
        return state.showPrivacy;
    },
    navBarHeight(state) {
        return state.navHeight;
    }
};
export const mutations = {
    setAccessToken(state, token) {
        state.accessToken = token;
    },
    destroyAccessToken(state) {
        state.accessToken = null;
    },
    setUser(state, user) {
        state.user = user;
    },
    confirmUser(state) {
        state.user.hasConfirmed = true;
    },
    destroyUser(state) {
        state.user = null;
    },
    addFlashMessage(state, {message, index}) {
        state.flashMessages.push({
            index: index,
            level: message.level,
            body: message.body
        });
    },
    removeFlashMessage(state, index) {
        state.flashMessages = state.flashMessages.filter(function (object) {
            return object.index !== index;
        });
    },
    setIsSubmitting(state) {
        state.isSubmitting = true;
    },
    unsetIsSubmitting(state) {
        state.isSubmitting = false;
    },
    toggleTerms(state) {
        state.showTerms = !state.showTerms;
    },
    togglePrivacy(state) {
        state.showPrivacy = !state.showPrivacy;
    },
    setNavHeight(state, height) {
        state.navHeight = height;
    },
};
export const actions = {
    nuxtServerInit({commit}, {req}) {
        let accessToken = null;
        let user = null;

        if (req.headers.cookie) {
            const parsed = cookieparser.parse(req.headers.cookie);
            try {
                accessToken = JSON.parse(parsed.accessToken);
                user = JSON.parse(parsed.user);
            } catch (err) {
                // No valid cookie found
            }
        }

        if (accessToken) {
            commit('setAccessToken', accessToken);
        }
        if (user) {
            commit('setUser', user);
        }
    },
    registerUser({commit, dispatch}, data) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');
            this.$axios.$post('/auth/register/', {
                email: data.email,
                name: data.name,
                password: data.password,
                password_confirmation: data.passwordConfirmation,
            })
                .then(response => {
                    dispatch('addFlashMessage', {
                        level: 'success',
                        body: 'You registered successfully!'
                    });
                    resolve();
                })
                .catch(error => {
                    dispatch('addFlashMessage', {
                        level: 'danger',
                        body: 'Oh no, something went wrong! We are working on fixing it. Please try again later.'
                    });
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    loginUser({commit, dispatch}, data) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');

            this.$axios.$post('/auth/login/', {
                username: data.email,
                password: data.password
            })
                .then(response => {
                    const token = response.access_token;
                    commit('setAccessToken', token);
                    Cookie.set('accessToken', JSON.stringify(response.access_token));
                    dispatch('setAuthenticationHeader');
                    dispatch('getUser');
                    dispatch('addFlashMessage', {
                        level: 'success',
                        body: 'You are now signed in!'
                    });
                    resolve();
                })
                .catch(error => {
                    dispatch('addFlashMessage', {
                        level: 'danger',
                        body: 'Oh no! Those credentials did not match our records. Please try again.'
                    });
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    confirmUser({commit, dispatch}, token) {
        commit('setIsSubmitting');
        dispatch('setAuthenticationHeader');
        this.$axios.$post('/auth/confirm/', {
            confirmation_token: token
        })
            .then(response => {
                commit('confirmUser');
                dispatch('addFlashMessage', {
                    level: 'success',
                    body: 'Awesome! Your account has been confirmed successfully!'
                });
            })
            .catch(error => {
                dispatch('addFlashMessage', {
                    level: 'danger',
                    body: 'Oh no, that token is invalid! Request a new one and try again.'
                });
            })
            .finally(() => {
                commit('unsetIsSubmitting');
            });
    },
    resendConfirmation({commit, dispatch}) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');
            dispatch('setAuthenticationHeader');
            this.$axios.$post('/auth/resend-confirm/')
                .then(response => {
                    dispatch('addFlashMessage', {
                        level: 'success',
                        body: 'A new confirmation email has been sent, check your emails and follow the link to confirm.'
                    });
                    resolve();
                })
                .catch(error => {
                    dispatch('addFlashMessage', {
                        level: 'danger',
                        body: 'We have already sent you a confirmation email recently, check your emails and follow the link to confirm.'
                    });
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    getUser({commit, dispatch}) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');
            dispatch('setAuthenticationHeader');
            this.$axios.$get('/user/')
                .then(response => {

                    const preparedUser = {
                        email: response.user.email,
                        name: response.user.name,
                        avatar: response.user.avatar,
                        hasConfirmed: !!response.user.email_verified_at
                    };

                    commit('setUser', preparedUser);
                    Cookie.set('user', JSON.stringify(preparedUser));
                    resolve();
                })
                .catch(error => {
                    this.$router.push('/logout');
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    logoutUser({commit, dispatch}) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');
            dispatch('setAuthenticationHeader');
            this.$axios.$post('/auth/logout/')
                .then(response => {
                    dispatch('destroyUser');
                    dispatch('addFlashMessage', {
                        level: 'success',
                        body: 'You have been logged out, thanks for stopping by.'
                    });
                    resolve();
                })
                .catch(error => {
                    commit('destroyAccessToken');
                    reject(error);
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    checkEmailAvailability({commit}, email) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');
            this.$axios.$get('/auth/email/' + email)
                .then(response => {
                    resolve(response.is_taken);
                })
                .catch(error => {
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    setAuthenticationHeader({state}) {
        this.$axios.setHeader("Authorization", "Bearer " + state.accessToken);
    },
    addFlashMessage({commit}, message) {
        const index = Math.random();
        commit('addFlashMessage', {message, index});

        setTimeout(function () {
            commit('removeFlashMessage', index);
        }, 6000)
    },
    removeFlashMessage({commit}, index) {
        commit('removeFlashMessage', index);
    },
    destroyUser({commit}) {
        Cookie.remove('accessToken');
        Cookie.remove('user');
        commit('destroyAccessToken');
        commit('destroyUser');
    },
    getProductTypes({commit}) {
        return new Promise((resolve, reject) => {
            commit('setIsSubmitting');
            this.$axios.$get('/product/type')
                .then(response => {
                    resolve(response.product_types);
                })
                .catch(error => {
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    getProducts({commit, dispatch}, type) {
        return new Promise((resolve, reject) => {
            dispatch('setAuthenticationHeader');
            commit('setIsSubmitting');
            this.$axios.$get('/product/' + type)
                .then(response => {
                    resolve(response.products);
                })
                .catch(error => {
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    },
    favoriteProduct({commit, dispatch}, product) {
        return new Promise((resolve, reject) => {
            if (this.getters.isLoggedIn) {
                dispatch('setAuthenticationHeader');
                commit('setIsSubmitting');
                this.$axios.$post('/product/favorite/' + product)
                    .then(response => {
                        dispatch('addFlashMessage', {
                            level: 'success',
                            body: 'The product has been added to your likes.'
                        });
                        resolve();
                    })
                    .catch(error => {
                        dispatch('addFlashMessage', {
                            level: 'danger',
                            body: 'Oh no! Something went wrong, the product was not added to your likes. Try again later.'
                        });
                        reject();
                    })
                    .finally(() => {
                        commit('unsetIsSubmitting');
                    });
            } else {
                dispatch('addFlashMessage', {
                    level: 'warning',
                    body: 'You have to be signed in to favorite products. Login or register and try again.'
                });
                reject();
            }
        });
    },
    unfavoriteProduct({commit, dispatch}, product) {
        return new Promise((resolve, reject) => {
            if (this.getters.isLoggedIn) {
                dispatch('setAuthenticationHeader');
                commit('setIsSubmitting');
                this.$axios.$delete('/product/favorite/' + product)
                    .then(response => {
                        dispatch('addFlashMessage', {
                            level: 'success',
                            body: 'The product has been removed from your likes.'
                        });
                        resolve();
                    })
                    .catch(error => {
                        dispatch('addFlashMessage', {
                            level: 'danger',
                            body: 'Oh no! Something went wrong, the product was not removed from your likes. Try again later.'
                        });
                        reject();
                    })
                    .finally(() => {
                        commit('unsetIsSubmitting');
                    });
            } else {
                dispatch('addFlashMessage', {
                    level: 'warning',
                    body: 'You have to be signed in to favorite or unfavorite products. Login or register and try again.'
                });
                reject();
            }
        });
    },
    getFavoritedProducts({commit, dispatch}) {
        return new Promise((resolve, reject) => {
            dispatch('setAuthenticationHeader');
            commit('setIsSubmitting');
            this.$axios.$get('/user/favorites')
                .then(response => {
                    resolve(response.products);
                })
                .catch(error => {
                    dispatch('addFlashMessage', {
                        level: 'danger',
                        body: 'Oh no! Something went wrong, the product was not removed from your likes. Try again later.'
                    });
                    reject();
                })
                .finally(() => {
                    commit('unsetIsSubmitting');
                });
        });
    }
};
