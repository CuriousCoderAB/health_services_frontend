# frontend_web

> Frontend web application to consume the `healthservices` api.

## Build Setup

### Google Maps
To have google maps work do the following:
* Copy the file `assets/keys.json.example` as `assets/keys.json`
* Add your google maps api key as the value for `googleApiKey` in `assets/keys.json`

*** PLEASE NOTE: For the features to work have the health services api running and set the url in the `nuxt.config.js` file ***

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
